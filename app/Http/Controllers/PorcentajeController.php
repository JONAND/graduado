<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PorcentajeController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function aumentar(Request $request)
    {
        $datos = [
            'japo_porcentaje' => 'required| max:3 |'
        ];
        $this->validate($request,$datos);

        $traerporcentaje = $request->get('japo_porcentaje');
        $dividirporcentaje  = $traerporcentaje/100;
        // return $datosPorcentaje;

        $consulta = DB::update('update libros set japo_precio_venta = (japo_precio_venta + (japo_precio_venta*'.$dividirporcentaje.')) ');

        $porcentajeAumentado = $dividirporcentaje*100;
        return redirect('libro')->with([
            'Mensaje' => 'Se sumo un '.$porcentajeAumentado. ' % a todos los libros'
        ]); 
    }

    public function disminuir(Request $request)
    {
     
        $datos = [
            'japo_porcentaje' => 'required'
        ];
        $this->validate($request,$datos);

        $traerporcentaje = $request->get('japo_porcentaje');
        $dividirporcentaje  = $traerporcentaje/100;
     

        $consulta = DB::update('update libros set japo_precio_venta = (japo_precio_venta - (japo_precio_venta*'.$dividirporcentaje.')) ');

        $porcentajeDisminuido = $dividirporcentaje*100;
        return redirect('libro')->with([
            'Mensaje' => 'Se resto un '.$porcentajeDisminuido. ' % a todos los libros'
        ]); 
    }

 
  

 
  
}
