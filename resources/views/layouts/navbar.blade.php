  <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav level-1">
              
              
              
              
                <li class="sidenav-heading">Mantenimientos</li>

              @if(Auth::user()->rol->japo_nombre == 'ADMINISTRADOR')  

                <li class="sidenav-item has-subnav">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-works">&#110;</span>
                    <span class="sidenav-label">Usuarios</span>
                  </a>
                  <ul class="sidenav level-2 collapse">
                    <li class="sidenav-heading">Usuarios</li>
                    <li><a href="{{ route('usuario.index') }}">Listado</a></li>
                    
                  </ul>
                </li>
                <li class="sidenav-item has-subnav open active">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-works">&#97;</span>
                    <span class="sidenav-label">Roles</span>
                  </a>
                  <ul class="sidenav level-2 collapse">
                    <li class="sidenav-heading">Roles</li>
                    <li><a href="{{ route('rol.index') }}">Listado</a></li>
                   
                  </ul>
                </li>

                @endif
                <li class="sidenav-item has-subnav">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-works">&#61;</span>
                    <span class="sidenav-label">Libros</span>
                  </a>
                  <ul class="sidenav level-2 collapse">
                    <li class="sidenav-heading">Libros</li>
                    <li><a href="{{ route('libro.index') }}">Listado</a></li>
                  
                  </ul>
                </li>
                <li class="sidenav-item has-subnav">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-works">&#38;</span>
                    <span class="sidenav-label">Autores</span>
                  </a>
                  <ul class="sidenav level-2 collapse">
                    <li class="sidenav-heading">Autores</li>
                    <li><a href="{{ route('autor.index') }}"">Listado </a></li>
                   
                  </ul>
                </li>
                
                <li class="sidenav-item has-subnav">
                  <a href="#multi-level-menu" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-works">&#103;</span>
                    <span class="sidenav-label">Editoriales </span>
                  </a>
                  <ul class="sidenav level-2 collapse">
                    <li class="sidenav-heading">Editoriales </li>
                    <li class="sidenav-item has-subnav">
                      <a href="{{ route('editorial.index') }}">
                        Listado
                      </a>
                    
                    </li>
                   
                  </ul>
                </li>
               
              
             
               
               
                
              
              
              
            
              
              </ul>
            </nav>
          </div>
        </div>
      </div>