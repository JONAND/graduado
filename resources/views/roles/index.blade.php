@extends('layouts.plantilla')

@section('mensaje')

	
	  @if(Session::has('Mensaje'))  
	      <div class="alert alert-warning alert-dismissible mt-3">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h5><i class="icon fas fa-warning"></i> Alert!</h5>
	        {{ Session::get('Mensaje') }}
	      </div>
	    @endif
@endsection
@section('content')

 <div class="row gutter-xs">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                  <button class="btn btn-outline-warning" data-toggle="modal" data-target="#warningModalAlert" 		type="button">Agregar</button>
             	 </div>
                   
                  </div>
                  <strong>Mantenimiento de Roles</strong>
                </div>
                <div class="card-body">
                  <table id="demo-datatables-5" class="table table-striped table-bordered table-nowrap dataTable" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Rol</th>                                 
	                    <th >Estado</th>
	                    <th>Eliminar</th>
	                    <th>Editar</th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach($leer as $recorrer)
                        <tr>
                            <td>{{ $recorrer->japo_nombre }}</td>
                            <td>{{ $recorrer->japo_estado }}</td>
                            <td>
                                <form action="{{ route('rol.destroy',$recorrer->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                      <button class="btn btn" style="border:none; background:white;" onclick="return confirm('Desea Eliminar?')">
                                      	
					                      <span class="icon icon-trash"></span>
					                      <span class="caption">trash</span>
					                    
                                      </button>
                                     
                                </form> 
                                
                            </td>
                            <td>  
                             

                                                     
                                <a class="btn btn-info btn-icon" href="{{ route('rol.edit',$recorrer->id) }}">   <span class="icon icon-edit"></span>
                     			      <span class="caption">edit</span>
                     			    </a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                  
                  </table>
                </div>
              </div>
            </div>
          </div>



         <!--modal para agregar -->

         <div id="warningModalAlert" tabindex="-1" role="dialog" class="modal fade">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">
	              <span aria-hidden="true">×</span>
	              <span class="sr-only">Close</span>
	            </button>
	          </div>
	          <div class="modal-body">
	            <div class="text-center">
	           
	              <h3 class="text-warning">Nuevo Rol</h3>
	                 <form role="form"  action="{{ route('rol.store') }}"  method="post">
		                @csrf
		                        <!--Static-->
		                 @include('roles.form',['mensaje'=> 'crear'])
		                
		              </form>
	              
	            </div>
	          </div>
	         
	        </div>
	      </div>
	    </div>



          <div class="modal fade" id="demo-default-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Nuevo Rol</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form"  action="{{ route('rol.store') }}"  method="post">
                @csrf
                        <!--Static-->
                 @include('roles.form',['mensaje'=> 'crear'])
                
              </form>
            </div>

             
              
              
             
            </div>
        </div>
    </div>
@endsection