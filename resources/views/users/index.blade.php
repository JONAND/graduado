@extends('layouts.plantilla')

@section('mensaje')

	
	  @if(Session::has('Mensaje'))  
	      <div class="alert alert-warning alert-dismissible mt-3">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h5><i class="icon fas fa-warning"></i> Alert!</h5>
	        {{ Session::get('Mensaje') }}
	      </div>
	    @endif
@endsection
@section('content')

 <div class="row gutter-xs">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                   <a href="{{ route('usuario.create') }}" class="btn btn-outline-warning" >Agregar</a> 
             	 </div>
                   
                  </div>
                  <strong>Mantenimiento de Usuarios</strong>
                </div>
                <div class="card-body">
                  <table id="demo-datatables-5" class="table table-striped table-bordered table-nowrap dataTable" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                      <th>Rol</th>                                 
                      <th >Nombres</th>
                      <th >Email</th>
                      <th>Dni</th>
                      <th>Sexo</th>
                      <th>Direccion</th>
                      <th>Telefono</th>
                      <th>Estado</th>
                      <th>Foto</th>
                      <th>Eliminar</th>
                      <th>Editar</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($leer as $recorrer)
                          <tr>
                              <td>{{ isset($recorrer->rol->japo_nombre) ? $recorrer->rol->japo_nombre : '' }}</td>
                              <td>{{ $recorrer->name }}</td>
                              <td>{{ $recorrer->email }}</td>
                              <td>{{ $recorrer->dni }}</td>
                              <td>{{ $recorrer->sexo }}</td>
                              <td>{{ $recorrer->direccion }}</td>
                              <td>{{ $recorrer->telefono }}</td>
                              <td>{{ $recorrer->estado }}</td>
                              <td><img src="{{ asset('storage'.'/'.$recorrer->foto) }}" width="50" class="img-thumbnail"></td>
                              <td>
                                  <form action="{{ route('usuario.destroy',$recorrer->id) }}" method="post">
                                      @csrf
                                      @method('DELETE')
                                        <button class="btn " style="border:none; background:white;" onclick="return confirm('Desea Eliminar?')">
                                           <span class="icon icon-trash"></span>
                                           <span class="caption">Delete</span>

                                        </button>
                                  </form> 
                                  
                              </td>
                              <td>  
                               

                              
                             
                                  <a class="btn btn-info btn-icon" href="{{ route('usuario.edit',$recorrer->id) }}">
                                   <span class="icon icon-edit"></span>
                                <span class="caption">edit</span>
                                 
                                  </a>
                              </td>

                          </tr>
                          @endforeach
                    </tbody>
                  
                  </table>
                </div>
              </div>
            </div>
          </div>



@endsection