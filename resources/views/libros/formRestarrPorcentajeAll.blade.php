		<div class="form-group">
       		 <label class="col-md-4 control-label" for="demo-text-input">Porcentaje</label>
            <div class="col-md-8">
                <input type="text" required="" name="japo_porcentaje"    class="form-control" placeholder="Ingrese Procentaje Ejem. 10,20">
                @error('japo_porcentaje')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>
        </div>

         <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input type="submit" onclick="return confirm('Desea cambiar el porcentaje?')" class="btn btn-warning"  value=" Disminuir Porcentaje del libro" name="">
        
   		 </div>