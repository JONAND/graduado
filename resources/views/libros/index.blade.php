@extends('layouts.plantilla')

@section('mensaje')

  
    @if(Session::has('Mensaje'))  
        <div class="alert alert-warning alert-dismissible mt-3">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><i class="icon fas fa-warning"></i> Alert!</h5>
          {{ Session::get('Mensaje') }}
        </div>
      @endif
@endsection
@section('content')

 <div class="row gutter-xs">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                 <a href="{{ route('libro.create') }}" class="btn btn-outline-warning">Agregar</a>
                  @auth 
                    @if(Auth::user()->rol->japo_nombre == 'ADMINISTRADOR') 
                      <button data-target="#modal-sumar" data-toggle="modal" class="btn btn-info btn-rounded">Aumentar Precio</button>

                      <button data-target="#modal-restar" data-toggle="modal" class="btn btn-danger btn-rounded">Restar Precio
                      </button>
                    @endif
                  @endauth   

               </div>
                   
                  </div>
                  <strong>Mantenimiento de Libros</strong>
                </div>
                <div class="card-body">
                  <table id="demo-datatables-5" class="table table-striped table-bordered table-nowrap dataTable" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Autor</th>  
                        <th>Editorial</th>                                
                        <th >ISBN</th>
                        <th >Titulo</th>
                        <th >Año</th>
                        <th >Precio de venta</th>
                        <th >Otros autores</th>
                        <th>Eliminar</th>
                        <th>Editar</th>

                         @auth @if(Auth::user()->rol->japo_nombre == 'ADMINISTRADOR')

                         <th>Porcentaje</th>
                        @endif
                        @endauth
                      </tr>
                    </thead>
                    <tbody>
                     @foreach($leer as $recorrer)
                        <tr>
                             <td> {{ isset($recorrer->autor->japo_nombres) ? $recorrer->autor->japo_nombres .' '. $recorrer->autor->japo_apellidos   : '' }} 
                                        </td>
                                        <td>{{ isset($recorrer->editorial->japo_nombre) ? $recorrer->editorial->japo_nombre : '' }}
                                        </td>

                                        <td>{{ $recorrer->japo_ISBN }}</td>
                                        <td>{{ $recorrer->japo_titulo }}</td>
                                        <td>{{ $recorrer->japo_anio }}</td>
                                        <td>{{ $recorrer->japo_precio_venta }}</td>
                                        <td>{{ $recorrer->japo_otros_autores }}</td>

                            <td>
                                <form action="{{ route('libro.destroy',$recorrer->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                      <button class="btn btn" style="border:none; background:white;" onclick="return confirm('Desea Eliminar?')">
                                        
                                <span class="icon icon-trash"></span>
                                <span class="caption">trash</span>
                              
                                      </button>
                                     
                                </form> 
                                
                            </td>
                            <td>  
                             
                                                   
                                <a class="btn btn-info btn-icon" href="{{ route('libro.edit',$recorrer->id) }}">   <span class="icon icon-edit"></span>
                                <span class="caption">edit</span>
                              </a>
                            </td>

                             @if(Auth::user()->rol->japo_nombre == 'ADMINISTRADOR')

                                  <td>
                                      <a href="{{ route('aumentopreciolibro.edit',$recorrer->id) }}" class="btn">
                                            <span class="icon icon-chevron-circle-up"></span>
                     
                                      </a>
                                  @if($recorrer->japo_precio_venta > 1)    
                                      <a href="{{ route('restapreciolibro.edit',$recorrer->id) }}" class="btn ">
                                        <span class="icon icon-chevron-circle-down"></span>
                                  @else
                                      <span class="text-danger">Precio muy bajo no disminuya</span>        
                                  @endif        
                                      </a>
                                  </td>
                              @endif 

                        </tr>
                        @endforeach
                    </tbody>
                  
                  </table>
                </div>
              </div>
            </div>
          </div>



         <!--modal para agregar  PRECIO A  TODOS LOS LIBROS-->

      <!-- MODALES -->

  

    <div class="modal fade" id="modal-sumar" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title text-center">Aumentar porcentaje a todos los Libros </h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                     <form action="{{ route('porcentajeall.aumentar') }}"  method="post" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('libros.formAumentarPorcentajeAll')
                                    
                     </form>
                   
                </div>

                <!--Modal footer-->
             
            </div>
        </div>
    </div>

     <div class="modal fade" id="modal-restar" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title text-center">Disminuir porcentaje a todos los Libros </h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                     <form action="{{ route('porcentajeall.disminuir') }}" onsubmit="return validacionPorcentaje()"  method="post" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('libros.formRestarrPorcentajeAll')
                                    
                     </form>
                   
                </div>

                <!--Modal footer-->
             
            </div>
        </div>
    </div>
@endsection