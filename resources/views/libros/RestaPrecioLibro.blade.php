@extends('layouts.plantilla')



  

@section('content')
  
     <div class="col-lg-12 ">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Restar valor del libro {{ $actualizar->japo_titulo }}
                  Valor $ {{ $actualizar->japo_precio_venta }}
                </h3>

            </div>


       
            <!--===================================================-->
            <form  action="{{ route('restapreciolibro.update', $actualizar->id) }}"   method="post" class="panel-body form-horizontal form-padding">
              @csrf
              @method('PUT')
              @include('libros.formDisminuirPorcentaje',['mensaje'=>'restar'])
             </div>
            </form>
            <!--===================================================-->
            <!-- END BASIC FORM ELEMENTS -->


        </div>
    </div>

@endsection