@extends('layouts.plantilla')

@section('mensaje')

	
	  @if(Session::has('Mensaje'))  
	      <div class="alert alert-warning alert-dismissible mt-3">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h5><i class="icon fas fa-warning"></i> Alert!</h5>
	        {{ Session::get('Mensaje') }}
	      </div>
	    @endif
@endsection
@section('content')

 <div class="row gutter-xs">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                <a href="{{ route('editorial.create') }}" class="btn btn-outline-warning ">Agregar</a>
             	 </div>
                   
                  </div>
                  <strong>Mantenimiento de Editoriales</strong>
                </div>
                <div class="card-body">
                  <table id="demo-datatables-5" class="table table-striped table-bordered table-nowrap dataTable" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th> Editorial</th>                                 
	                      <th >Email</th>
	                      <th >Estado</th>
	                      <th>Eliminar</th>
	                      <th>Editar</th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach($editorial as $editoriales)
                        <tr>
                            <td>{{ $editoriales->japo_nombre }}</td>
                            <td>{{ $editoriales->japo_email }}</td>
                            <td>{{ $editoriales->japo_estado }}</td>

                            <td>
                                <form action="{{ route('editorial.destroy',$editoriales->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                      <button class="btn btn" style="border:none; background:white;" onclick="return confirm('Desea Eliminar?')">
                                      	
					                      <span class="icon icon-trash"></span>
					                      <span class="caption">trash</span>
					                    
                                      </button>
                                     
                                </form> 
                                
                            </td>
                            <td>  
                             

                                                     
                                <a class="btn btn-info btn-icon" href="{{ route('editorial.edit',$editoriales->id) }}">   <span class="icon icon-edit"></span>
                     			      <span class="caption">edit</span>
                     			    </a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                  
                  </table>
                </div>
              </div>
            </div>
          </div>



@endsection